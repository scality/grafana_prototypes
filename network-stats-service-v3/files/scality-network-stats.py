#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:smarttab:expandtab:softtabstop=4:autoindent
import sys
sys.dont_write_bytecode = True

# TODO:
# start as a service
# calcul the derivations before sending it to ES
# Clean the values

## prerequisites
# sudo pip install elasticsearch

from datetime import datetime
from elasticsearch import Elasticsearch
from elasticsearch import ElasticsearchException
from elasticsearch.helpers import bulk
from pprint import pprint as pp
import socket
import yaml
import os
from threading import Thread, current_thread
import Queue
import traceback
import time
import copy
from decimal import getcontext, Decimal
getcontext().prec = 3


# used to make global variables
class _Config(object):
   def __init__(self):
      self.hostname = socket.gethostname().split('.', 1)[0]
      self.sagent_file = '/etc/sagentd.yaml'
      self.grain_file = "/etc/salt/grains"
      self.svsd_file = "/etc/svsd.conf"
      self.PROC_TCP = "/proc/net/tcp"
      self.STATE = {
         '01': 'ESTABLISHED',
         '02': 'SYN_SENT',
         '03': 'SYN_RECV',
         '04': 'FIN_WAIT1',
         '05': 'FIN_WAIT2',
         '06': 'TIME_WAIT',
         '07': 'CLOSE',
         '08': 'CLOSE_WAIT',
         '09': 'LAST_ACK',
         '0A': 'LISTEN',
         '0B': 'CLOSING'
      }

      self.sagent_conf = ""
      self.es_nodes = ""
      self.svsd_conf = ""
      self.grain_conf = ""
      self.server_roles = ""  # Initialized in main
      self.es = "" # Initialized in main
      self.ticktime = 10

      # Index creation settings
      self.index_name = 'network-stats-v3'
      self.index_alias = 'network-stats-v3'
      self.index_settings = {
         "settings": {
            "number_of_shards": 3,
            "number_of_replicas": 1,
            "index.codec": "best_compression",
            "analysis": {
               "analyzer": {
                  "casesensitive": {
                     "type": "custom",
                     "tokenizer": "standard"
                  }
               }
            }
         },
         "mappings": {
            self.index_alias: {
               "properties": {
                  "@hostname": {
                     "type": "string",
                     "index": "not_analyzed"
                  },
                  "@roles": {
                     "type": "string",
                     "analyzer": "casesensitive",
                     "index": "analyzed"
                  },
                  "@zone": {
                     "type": "string",
                     "index": "not_analyzed"
                  },
                  "@svsd_rip": {
                     "type": "string",
                     "index": "not_analyzed"
                  },
                  "@tag": {
                     "type": "string",
                     "index": "not_analyzed"
                  },
                  "@iface": {
                     "type": "string",
                     "index": "not_analyzed"
                  },
                  "@TI": {
                     "type": "string",
                     "index": "not_analyzed"
                  }
               }
            }
         }
      }
      self.prev = ""


config = _Config()

try:
   with open(config.sagent_file, 'r') as data:
      config.sagent_conf = yaml.load(data)
except IOError as e:
   print(e)
   sys.exit(1)

try:
   with open(config.grain_file, 'r') as data:
      config.grain_conf = yaml.load(data)
except IOError as e:
   print(e)
   sys.exit(1)

# Get ES hosts
config.es_nodes = config.sagent_conf['stats_poll_hosts']

if os.path.isfile(config.svsd_file):
   try:
      with open(config.svsd_file, 'r') as data:
         config.svsd_conf = yaml.load(data)
   except IOError as e:
      print(e)
      pass


def parse_proc_tcp(ltime):
   def _remove_empty(array):
      return [x for x in array if x != '']

   net_stats = {}
   for state in config.STATE.values():
      # Counter reset
      net_stats.update({state: 0})

   # Read the table of tcp connections
   try:
      lines = open(config.PROC_TCP, 'r').readlines()
   except IOError as err:
      print(err)
      return False

   # We start at 1 to remove the header
   for line in lines[1:]:
      # Split lines and remove empty spaces.
      line_array = _remove_empty(line.split(' '))
      state = config.STATE[line_array[3]]
      net_stats[state] += 1
   net_stats.update({
      '@hostname': config.hostname,
      '@timestamp': ltime,
      "@tag": "tcpstat"
   })
   return [net_stats]


def parse_netstat(ltime):
   try:
      lines = open("/proc/net/netstat", "r").readlines()
   except IOError as e:
      print(e)
      return False

   netstat = {"stat": {}}
   buf = []
   for idx, line in enumerate(lines):
      buf.append(line.split())
      if idx % 2:
         try:
            for entry in zip(*buf):
               v = 0
               try:
                  v = int(entry[1])
               except:
                  pass
               netstat["stat"].update({entry[0]: v})
         except ValueError:
            continue
   netstat.update({
      '@hostname': config.hostname,
      '@timestamp': ltime,
      "@tag": "netstat"
   })
   buf = []
   return [netstat]


def parse_netdev(ltime):
   try:
      lines = open("/proc/net/dev", "r").readlines()
   except IOError as e:
      print(e)
      return False

   arr_status = []
   columnLine = lines[1]
   _, receiveCols, transmitCols = columnLine.split("|")
   receiveCols = map(lambda a: "recv_" + a, receiveCols.split())
   transmitCols = map(lambda a: "trans_" + a, transmitCols.split())
   cols = receiveCols + transmitCols
   for line in lines[2:]:
      if line.find(":") < 0: continue
      face, data = line.split(":")
      numbers = [int(x) for x in data.split()]
      faceData = dict(zip(cols, numbers))
      faceData.update({
         '@iface': face.split()[0],
         '@hostname': config.hostname,
         '@timestamp': ltime,
         "@tag": "netdev"
      })
      arr_status.append(faceData)

   return arr_status


def parse_roles():
   new_roles = []
   if "roles" in config.grain_conf:
      for role in config.grain_conf['roles']:
         if role == "ROLE_STORE":
            new_roles.append("Storage")
         if role == "ROLE_ELASTIC":
            new_roles.append("ElasticSearch")
         if role == "ROLE_CONN_SOFS":
            new_roles.append("Sfused")
         if role == "ROLE_CONN_NFS":
            new_roles.append("NFS")
         if role == "ROLE_CONN_CIFS":
            new_roles.append("CIFS")
         if role == "ROLE_CONN_CDMI":
            new_roles.append("CDMI")
         if role == "ROLE_SVSD":
            new_roles.append("SVSD")
         if role == "ROLE_CONN_SPROXYD":
            new_roles.append("Sproxyd")
         if role == "ROLE_CONN_RS2":
            new_roles.append("RS2")
         if role == "ROLE_S3":
            new_roles.append("S3")
         if role == "ROLE_ZK":
            new_roles.append("Zookeeper")
         if role == "ROLE_SUP":
            new_roles.append("Supervisor")

   roles = ",".join([str(x) for x in new_roles])
   server_roles = { '@roles': roles }

   if config.svsd_conf:
      try:
         if 'zones' in config.svsd_conf and 'name' in config.svsd_conf[
               'zones'][0]:
            zone = config.svsd_conf['zones'][0]['name']
            rip = config.svsd_conf['zones'][0]['rip']
            server_roles.update({'@svsd_rip': rip, '@zone': zone})
      except IOError as e:
         print(e)
         pass

   server_roles.update({'@TI': "{0}s".format(config.ticktime)})

   return server_roles


def create_index(index, alias):
   idx = get_index_date()
   try:
      if not config.es.indices.exists(index=idx):
         # ignore 400 cause by IndexAlreadyExistsException when creating an index
         config.es.indices.create(index=idx, body=config.index_settings, ignore=400)
         config.es.indices.put_alias(index=idx, name=alias)
   except ElasticsearchException as e:
      print('ES Error: {0}'.format(e))
      return False


def get_now():
   return datetime.utcnow()


def get_timestamp():
   now = get_now()
   return now
   #return now.strftime("%Y-%m-%dT%H:%M:%S") + ".%03d" % (now.microsecond / 1000) + "Z"


def get_index_date():
   now = get_now()
   today = now.strftime("%Y-%m-%d")
   return '{name}-{date}'.format(name=config.index_name, date=today)



def process():
   global config

   # Get the timestamp at the beginning
   timestamp = get_timestamp()
   index_date = get_index_date()

   # Data to send to ES
   system_stats = []

   # Grabs stats
   netdev_info = parse_netdev(timestamp)
   if netdev_info:
      system_stats.extend(netdev_info)
   netstat_info = parse_netstat(timestamp)
   if netstat_info:
      system_stats.extend(netstat_info)
   tcp_info = parse_proc_tcp(timestamp)
   if tcp_info:
      system_stats.extend(tcp_info)

   if config.server_roles:
      for stat in system_stats:
         stat.update(config.server_roles)

   if not config.prev:
      config.prev = system_stats
      return False
   else:
      # Compute the derivations
      good_stats = get_derive(config.prev, system_stats)
      #pp(good_stats)

      # Replace the prev stats
      config.prev = system_stats

      # Create the index
      create_index(config.index_name, config.index_alias)

      # Ingest stats
      try:
         bulk(
            config.es,
            good_stats,
            index=index_date,
            doc_type=config.index_name,
            raise_on_error=False)
      except ElasticsearchException as e:
         print('ES Error: {0}'.format(e))

def get_derive(prev, current):
   cur = copy.deepcopy(current)  # we don't want to modify original doc
   i = 0
   while i < len(prev):
      if "netdev" in prev[i]["@tag"]:
         iface = prev[i]["@iface"]
         j = 0
         while j < len(cur):
            if "netdev" in cur[j]["@tag"] and iface == cur[j]["@iface"]:
               dt1 = prev[i]["@timestamp"]
               dt2 = cur[j]["@timestamp"]
               dt3 = dt2 - dt1
               difftime = dt3.seconds * 1000000 + dt3.microseconds
               # fix timestamp format
               cur[j]["@timestamp"] = dt2.strftime("%Y-%m-%dT%H:%M:%S") + ".%03d" % (dt2.microsecond / 1000) + "Z"
               for elem in cur[j]:
                  if '@' in elem:
                     continue
                  # derivation
                  #cur[j][elem] = float((cur[j][elem] - prev[i][elem])) / difftime
                  cur[j][elem] = round_derive(cur[j][elem], prev[i][elem], difftime)
               break
            j += 1

      if "tcpstat" in prev[i]["@tag"]:
         j = 0
         while j < len(cur):
            if "tcpstat" in cur[j]["@tag"]:
               dt1 = prev[i]["@timestamp"]
               dt2 = cur[j]["@timestamp"]
               dt3 = dt2 - dt1
               difftime = dt3.seconds * 1000000 + dt3.microseconds
               # fix timestamp format
               cur[j]["@timestamp"] = dt2.strftime("%Y-%m-%dT%H:%M:%S") + ".%03d" % (dt2.microsecond / 1000) + "Z"

               # There is no need to compute the derive here
               break
            j += 1

      if "netstat" in prev[i]["@tag"]:
         j = 0
         while j < len(cur):
            if "netstat" in cur[j]["@tag"]:
               dt1 = prev[i]["@timestamp"]
               dt2 = cur[j]["@timestamp"]
               dt3 = dt2 - dt1
               difftime = dt3.seconds * 1000000 + dt3.microseconds
               # fix timestamp format
               cur[j]["@timestamp"] = dt2.strftime("%Y-%m-%dT%H:%M:%S") + ".%03d" % (dt2.microsecond / 1000) + "Z"
               for elem in cur[j]["stat"]:
                  # derivation
                  try:
                     #cur[j]["stat"][elem] = float((cur[j]["stat"][elem] - prev[i]["stat"][elem])) / difftime
                     cur[j]["stat"][elem] = round_derive(cur[j]["stat"][elem], prev[i]["stat"][elem], difftime, elem)
                  except:
                     pass
            j += 1
      i += 1
   return cur


def round_derive(cur, prev, difftime, elem=None):
   return (float(cur) - float(prev)) / (difftime / 1000 / 1000)



if __name__ == '__main__':
   # ES connection
   config.es = Elasticsearch(config.es_nodes, port="9200")

   # Get the roles
   config.server_roles = parse_roles()

   while True:
      thread_ = Thread(
         group = None,
         target = process,
         name = "process"
      )
      thread_.start()

      thread_.join()

      #print 'Waiting {0} seconds'.format(config.ticktime)
      time.sleep(config.ticktime)
