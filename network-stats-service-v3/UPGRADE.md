
Upgrade from network-stats-v2 to network-stats-v3:

# salt-cp '*' /root/scality-network-stats.service /etc/systemd/system/scality-network-stats.service
# salt '*' cmd.run 'rm -f /etc/cron.d/scality-network-stats-cron'
# salt '*' cmd.run 'rm -f /usr/local/bin/scality-network-stats.py'
# salt-cp '*' /root/scality-network-stats.py /usr/local/bin/scality-network-stats.py
# curl -XDELETE 'store01:9200/network-stats-v2*'
# salt '*' cmd.run 'chmod +x /usr/local/bin/scality-network-stats.py'
# salt '*' cmd.run 'systemctl daemon-reload'
# salt '*' service.enable scality-network-stats
# salt '*' service.start scality-network-stats


checks:
# salt '*' service.status scality-network-stats'
# curl -s http://localhost:80/api/v0.1/es_proxy/_cat/indices?v |grep network-stats
