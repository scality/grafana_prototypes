Install network-stats-v3:

Network-stats is included from RING v7.4.5
v4 is needed for ElasticSearch v6 (RING 7.4.4+)

If you are installing or upgrading a <= 7.4.3.0, here are the instructions:

Download the network-stats-service-v3 from git repo https://bitbucket.org/scality/grafana_prototypes
Upload the whole directory on the supervisor, e.g. as network-stats-service-v3

Copy the files and launch the service as such:

mkdir -p /srv/scality/salt/local/scality
mv network-stats-service-v3 /srv/scality/salt/local/scality/network-stats

salt '*' state.sls scality.network-stats
salt -G roles:ROLE_SUP state.sls scality.network-stats.sup

All done!

