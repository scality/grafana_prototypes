#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=4:shiftwidth=4:smarttab:expandtab:softtabstop=4:autoindent

## requisites
# sudo pip install elasticsearch

import sys
sys.dont_write_bytecode = True

import time
from datetime import datetime
from elasticsearch import Elasticsearch
from elasticsearch import ElasticsearchException
from elasticsearch.helpers import bulk
from pprint import pprint as pp
import socket
import yaml
import json
import os
import socket
from threading import Thread, current_thread
import Queue
import traceback
import time
import copy
from sagentd.polling import sfused

hostname = socket.gethostname().split('.', 1)[0]
sagent_file = '/etc/sagentd.yaml'
grain_file = "/etc/salt/grains"
svsd_file = "/etc/svsd.conf"
sfused_file = "/run/scality/connectors/sfused/stats/sparse_stats"
# Nb seconds between polling
ticktime = 5
prev_stats = []

# Lock a thread by name
thread_name_list = {}

try:
   with open(sagent_file, 'r') as data:
      sagent_conf = yaml.load(data)
except IOError:
   sys.exit(1)

try:
   with open(grain_file, 'r') as data:
      grain_conf = yaml.load(data)
except IOError:
   sys.exit(1)

if os.path.isfile(svsd_file):
   try:
      with open(svsd_file, 'r') as data:
         svsd_conf = yaml.load(data)
   except IOError:
      sys.exit(1)

try:
   es_nodes = sagent_conf['stats_poll_hosts']
except:
   # not initialized
   sys.exit(1)

# Manage several instances of sfused on same server
daemons = {}
for daemon in sagent_conf["daemons"]:
   if "sfused" in sagent_conf["daemons"][daemon]["type"]:
      daemons.update({daemon: sagent_conf["daemons"][daemon]})
polling_interfaces = []
for daemon in daemons:
   name = daemon
   sdaemon = sagent_conf["daemons"][name]
   poll = sfused.StatsPollSfused(
      name=name, daemon=sdaemon, stats_index=None, polling_period=None)
   # limit the files
   poll.TOMONITOR_MISC = ["stats_sparse*", "stats_sfused", "nfs_stats"]
   polling_interfaces.append({daemon: {"prev_stats": [], "poll_iface": poll}})

# Index creation settings
index_name = 'sfused-gauges'
index_alias = 'sfused-gauges'
index_settings = {
   "index_patterns": ["{0}-*".format(index_name)],
   "settings": {
      "number_of_shards": 6,
      "number_of_replicas": 1,
      "index.codec": "best_compression",
   },
   "mappings": {
      index_alias: {
         "dynamic_templates": [
            {
               "timestamps_as_date": {
                  "match_pattern": "regex",
                  "path_match": ".*timestamp",
                  "mapping": {
                     "type": "date"
                  }
               }
            },
            {
               "keyword": {
                   "analyzer": {
                      "casesensitive": {
                         "type": "string",
                         "tokenizer": "keyword",
                         "index": "not_analyzed"
                      }
                   }
                }
            },
            {
               "strings_not_analyzed": {
                  "match": "*",
                  "match_mapping_type": "string",
                  "mapping": {
                     "type": "keyword",
                     "index": "not_analyzed"
                  }
               }
            }
         ]
      },
      index_alias: {
         "properties": {
            "@roles": {
               "type": "string",
               "index": "not_analyzed"
            },
            "@TI": {
               "type": "string",
               "index": "not_analyzed"
            },
            "service": {
               "type": "string",
               "index": "not_analyzed"
            },
            "host": {
               "type": "string",
               "index": "not_analyzed"
            },
            "layer": {
               "type": "string",
               "index": "not_analyzed"
            }
         }
      }
   }
}

def create_index(es, index, alias):
   """
      Create index in ES
   """
   indice_name = '{name}-{ts}'.format(name=index, ts=today)

   try:
      if not es.indices.exists(index=indice_name):
         # ignore 400 cause by IndexAlreadyExistsException when creating an index
         es.indices.create(index=indice_name, body=index_settings, ignore=400)
         es.indices.put_alias(index=indice_name, name=alias)
   except ElasticsearchException as e:
      print 'ES Error: {0}'.format(e)
      return False

   return True

def process(es, name, poll, index_date):
   global polling_interfaces
   global thread_name_list
   thread_name = current_thread().getName()

   # Previous process for this thread not finished yet?
   if thread_name_list[thread_name]:
      return False
   thread_name_list[thread_name] = True

   # Index creation if needed
   create_index(es, index_name, index_alias)

   ltime = now.strftime(
      "%Y-%m-%dT%H:%M:%S") + ".%03d" % (now.microsecond / 1000) + "Z"
   polling_stats = poll.poll_misc()
   operations = ["read", "write", "delete", "count", "unlink", "st_read", "st_write", "st_uread", "st_uwrite", "st_rewrite", "st_delete", "nfs_read", "nfs_write", "nfs_remove"]
   mypolls = []
   for misc in polling_stats:
      for ope in operations:
         try:
            if ope == misc["operation"]:
               mypolls.append(misc)
         except:
            pass
   """
    polling_interfaces =
    [{
    'gauges-store-0-sfused': {
        'poll_iface': Sfused(10.200.5.104:7000,/run/scality/connectors/sfused),
        'prev_stats': []
        },
    ...
    }]
    """
   for i, iface in enumerate(polling_interfaces):
      for a in iface.keys():
         if a == name:
            if iface[name]["prev_stats"]:
               prev = iface[name]["prev_stats"]
               derivation = poll.derive(prev, mypolls)

               stats = []
               for i in derivation:
                  # in stats_sparse, we kee the stripe operations only
                  if 'stats_sparse:2' in i["layer"]:
                     i.update({'@view': "backend"})
                     if i["operation"] == "write" or i["operation"] == "read" or i["operation"] == "delete":
                        continue
                  elif 'stats_sfused' in i["layer"]:
                     i.update({'@view': "frontend"})
                  elif 'nfs_stats' in i["layer"]:
                     i.update({'@view': "frontend"})

                  i.update(server_groups)
                  i.update({'@timestamp': ltime})
                  # get the shortname
                  host = i.get("host").split('.')
                  i.update({"host": host[0]})
                  stats.append(i)

               # Ingest stats
               print "ingest"
               try:
                  bulk(
                     es,
                     stats,
                     index=index_date,
                     doc_type=index_name,
                     raise_on_error=False)
               except ElasticsearchException as e:
                  print 'ES Error: {0}'.format(e)
                  thread_name_list[thread_name] = False
               except Exception:
                  print "Generic Exception: {}".format(traceback.format_exc())
                  thread_name_list[thread_name] = False

            iface[name]["prev_stats"] = mypolls
            thread_name_list[thread_name] = False


def get_groups():
   server_roles = {'@TI': "{0}s".format(ticktime)}
   new_roles = []

   if grain_conf and "roles" in grain_conf:
      for role in grain_conf['roles']:
         if role == "ROLE_STORE":
            new_roles.append("Storage")
         if role == "ROLE_ELASTIC":
            new_roles.append("ElasticSearch")
         if role == "ROLE_CONN_SOFS":
            new_roles.append("Sfused")
         if role == "ROLE_CONN_NFS":
            new_roles.append("NFS")
         if role == "ROLE_CONN_CIFS":
            new_roles.append("CIFS")
         if role == "ROLE_CONN_CDMI":
            new_roles.append("CDMI")
         if role == "ROLE_SVSD":
            new_roles.append("SVSD")
         if role == "ROLE_CONN_SPROXYD":
            new_roles.append("Sproxyd")
         if role == "ROLE_CONN_S3":
            new_roles.append("S3")
         if role == "ROLE_CONN_RS2":
            new_roles.append("RS2")
         if role == "ROLE_ZK":
            new_roles.append("Zookeeper")
         if role == "ROLE_SUP":
            new_roles.append("Supervisor")

      roles = ",".join([str(x) for x in new_roles])
      server_roles.update({'@roles': roles})

   try:
      if svsd_conf:
         if 'zones' in svsd_conf and 'name' in svsd_conf['zones'][0]:
            zone = svsd_conf['zones'][0]['name']
            rip = svsd_conf['zones'][0]['rip']
            server_roles.update({'@svsd_rip': rip, '@zone': zone})
   except:
      pass

   return server_roles


if __name__ == '__main__':
   es = Elasticsearch(es_nodes, port="9200")

   server_groups = get_groups()

   for daemon in daemons:
      thread_name_list.update({daemon: False})

   while True:
      thread_list = []
      for daemon in daemons:
         name = daemon
         for i, val in enumerate(polling_interfaces):
            if name in val:
               daemon_poll_iface = val[name]["poll_iface"]
         if not daemon_poll_iface:
            print "name not found"
            sys.exit(0)

         now = datetime.utcnow()
         today = now.strftime("%Y-%m-%d")
         index_date = '{name}-{date}'.format(name=index_name, date=today)
         # bulk(es, [derivation], index = index_date, doc_type=index_name, raise_on_error = False)

         thread_ = Thread(
            group=None,
            target=process,
            name=name,
            args=(es, name, daemon_poll_iface, index_date))
         thread_.start()

         thread_list.append(thread_)

      for i, t in enumerate(thread_list):
         t.join()

      print 'Waiting {0} seconds'.format(ticktime)
      time.sleep(ticktime)
