#                       scality.network-stats
#                            |
#                 +----------+---------+
#                 |                    |
#                 |                 dashboard
#           +-----+-----+
#           |           |
#       installed  configured
#
#
# Example usage
# =============
#
# 1. Configure all servers to push traces to ES.
#
# .. code:: console
#
#   salt '*' state.sls scality.network-stats
#
# 2. Configure grafana and the curator on the supervisor
#   salt -G roles:ROLE_SUP state.sls scality.network-stats.sup
#
include:
  - .installed
  - .configured
