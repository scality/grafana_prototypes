#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:
# tabstop=4:shiftwidth=4:smarttab:expandtab:softtabstop=4:autoindent:foldmethod=indent
"""
    Try to get network stats and ingest into ElasticSearch
"""

import socket
import copy
import time
import os
import sys
from decimal import getcontext
from datetime import datetime
from threading import Thread
import yaml
from elasticsearch import Elasticsearch
from elasticsearch import ElasticsearchException
from elasticsearch.helpers import bulk

sys.dont_write_bytecode = True
getcontext().prec = 3

# global variables
SAGENTD_FILE = '/etc/sagentd.yaml'
SALT_GRAIN_FILE = "/etc/salt/grains"
SVSD_CONF_FILE = "/etc/svsd.conf"
PROC_NET_NETSTAT = "/proc/net/netstat"
PROC_NET_DEV = "/proc/net/dev"
PROC_NET_TCP_FILE = "/proc/net/tcp"
HOSTNAME = socket.gethostname().split('.', 1)[0]
# run process() every 10 seconds
TICKTIME = 10
# Name of the index (and alias probably)
ES_INDICE_NAME = "network-stats-v4"

# Table match
PROC_NET_TCP_STATE = {
    '01': 'ESTABLISHED',
    '02': 'SYN_SENT',
    '03': 'SYN_RECV',
    '04': 'FIN_WAIT1',
    '05': 'FIN_WAIT2',
    '06': 'TIME_WAIT',
    '07': 'CLOSE',
    '08': 'CLOSE_WAIT',
    '09': 'LAST_ACK',
    '0A': 'LISTEN',
    '0B': 'CLOSING'
}

# SALT known roles
KNOWN_SALT_ROLES = {
    "ROLE_STORE": "Storage",
    "ROLE_ELASTIC":"ElasticSearch",
    "ROLE_CONN_SOFS": "Sfused",
    "ROLE_CONN_NFS": "NFS",
    "ROLE_CONN_CIFS": "CIFS",
    "ROLE_CONN_CDMI": "CDMI",
    "ROLE_SVSD": "SVSD",
    "ROLE_CONN_SPROXYD": "Sproxyd",
    "ROLE_CONN_RS2": "RS2",
    "ROLE_S3": "S3",
    "ROLE_ZK": "Zookeeper",
    "ROLE_SUP": "Supervisor"
}

# used to make global updatable variables
class _Config(object):
    """
        Global settings.
        kind of global variables
    """
    def __init__(self):
        self.index_alias = ES_INDICE_NAME
        self.index_settings = {}
        self.previous_stats = None
        self.sagent_conf = None
        self.es_nodes = None
        self.svsd_conf = {}
        self.grain_conf = {}
        self.server_roles = None
        self.socket_elasticsearch = None
        self.latest_created_indice = None

    def elasticsearch_index_info(self):
        """
            ES index info
        """
        # Index creation settings
        self.index_settings = {
            "index_patterns": ["{0}-*".format(ES_INDICE_NAME)],
            "settings": {
                "number_of_shards": 3,
                "number_of_replicas": 1,
                "index.codec": "best_compression",
            },
            "mappings": {
                self.index_alias: {
                    "dynamic_templates": [{
                        "strings_as_keyword": {
                            "match_mapping_type": "string",
                            "mapping": {"type": "keyword"}
                        }
                    }],
                    "date_detection": False,
                    "properties": {
                        "@timestamp": {"type": "date"},
                        "@roles": {"type": "text", "fielddata": True},
                        "netdev": {
                            "properties": {
                                "@iface": {"type": "keyword"},
                                "@state": {"type": "keyword"}
                            }
                        },
                        "netstat": {
                            "properties": {
                                "@extname": {"type": "keyword"}
                            }
                        }
                    }
                }
            }
        }

    def init(self):
        """
            Get the needed system variables
        """

        self.elasticsearch_index_info()

        # try to identify server from salt grains if exists
        if os.path.isfile(SALT_GRAIN_FILE):
            try:
                with open(SALT_GRAIN_FILE, 'r') as data:
                    self.grain_conf = yaml.load(data, Loader=yaml.Loader)
            except IOError as err:
                print(err)
                sys.exit(1)

        # Get the elasticsearch hosts from sagentd.yaml file
        try:
            with open(SAGENTD_FILE, 'r') as data:
                self.sagent_conf = yaml.load(data, Loader=yaml.Loader)
            # Get ES hosts
            self.es_nodes = self.sagent_conf['stats_poll_hosts']
        except IOError as err:
            print(err)
            sys.exit(1)

        # Try to identify SVSD zones
        if os.path.isfile(SVSD_CONF_FILE):
            try:
                with open(SVSD_CONF_FILE, 'r') as data:
                    self.svsd_conf = yaml.load(data, Loader=yaml.Loader)
            except IOError as err:
                print(err)

CONFIG = _Config()
CONFIG.init()


def parse_proc_net_tcp():
    """
        Parse tcp state file
    """
    def _remove_empty(array):
        return [x for x in array if x != '']

    tcpstats = {"tcpstat": {}}
    # Counter reset
    for state in PROC_NET_TCP_STATE.values():
        tcpstats["tcpstat"].update({state: 0})

    # Read the table of tcp connections & remove header
    try:
        lines = open(PROC_NET_TCP_FILE, 'r').readlines()
    except IOError as err:
        print(err)
        return False

    # We start at 1 to remove the header
    for line in lines[1:]:
        # Split lines and remove empty spaces.
        line_array = _remove_empty(line.split(' '))
        state = PROC_NET_TCP_STATE[line_array[3]]
        tcpstats["tcpstat"][state] += 1

    return tcpstats


def parse_netstat():
    """
        Parse net state file
    """
    try:
        lines = open(PROC_NET_NETSTAT, "r").readlines()
    except IOError as err:
        print(err)
        return False

    buf = []
    result = []

    # stats lines work in pair, header + data
    for idx, line in enumerate(lines):
        buf.append(line.split())
        if idx % 2:
            # Name of the entries (TcpExt, IpExt,..)
            ext_name = buf[0][0]
            stats = {}
            for entry in zip(*buf):
                val = 0
                try:
                    val = int(entry[1])
                except ValueError:
                    continue
                stats.update({entry[0]: val})
            stats.update({
                "@extname": ext_name
            })
            result.append({"netstat": stats})
            # Reset buffer for the next pair of lines
            buf = []
    return result


def interface_status(iface):
    """
        Parse interface status
    """
    operstate = "/sys/class/net/{0}/operstate".format(iface)
    state = 'unknown'
    if os.path.isfile(operstate):
        try:
            state = [line.strip()
                     for line in open(operstate, "r").readlines()][0]
        except IOError as err:
            print(err)
            return False
    return state


def parse_netdev():
    """
        Parse net dev file
    """
    try:
        lines = open(PROC_NET_DEV, "r").readlines()
    except IOError as err:
        print(err)
        return False

    arr_status = []
    col_line = lines[1]
    _, receivecols, transmitcols = col_line.split("|")
    receivecols = map(lambda a: "recv_" + a, receivecols.split())
    transmitcols = map(lambda a: "trans_" + a, transmitcols.split())
    cols = receivecols + transmitcols
    for line in lines[2:]:
        if line.find(":") < 0:
            continue
        face, data = line.split(":")
        numbers = [int(x) for x in data.split()]
        facedata = {"netdev": dict(zip(cols, numbers))}
        facedata["netdev"].update({
            '@iface': face.split()[0],
            '@state': interface_status(face.split()[0])
        })
        arr_status.append(facedata)
    return arr_status


def parse_roles():
    """
        Try to guess the role and svsd zone
    """
    server_roles = {}
    new_roles = []
    if "roles" in CONFIG.grain_conf:
        for role in CONFIG.grain_conf['roles']:
            if role in KNOWN_SALT_ROLES:
                new_roles.append(KNOWN_SALT_ROLES[role])

    roles = ",".join([str(x) for x in new_roles])
    if roles:
        server_roles.update({'@roles': roles})

    if CONFIG.svsd_conf:
        try:
            if 'zones' in CONFIG.svsd_conf:
                if 'name' in CONFIG.svsd_conf['zones'][0]:
                    zone = CONFIG.svsd_conf['zones'][0]['name']
                    rip = CONFIG.svsd_conf['zones'][0]['rip']
                    server_roles.update({'@svsd_rip': rip, '@zone': zone})
        except IOError as err:
            print(err)

    return server_roles


def create_index():
    """
        Create index in ES
    """
    indice_name = get_indice_name_with_date()
    # Do not try to create the indice it already created
    if indice_name == CONFIG.latest_created_indice:
        return True

    try:
        # Even with the previous check, the indice may already exists
        if not CONFIG.socket_elasticsearch.indices.exists(index=indice_name):
            CONFIG.socket_elasticsearch.indices.create(
                index=indice_name, body=CONFIG.index_settings, ignore=400)
            CONFIG.socket_elasticsearch.indices.put_alias(index=indice_name, name=CONFIG.index_alias)
    except ElasticsearchException as err:
        print("ES Error: {0}".format(err))
        return False

    CONFIG.latest_created_indice = indice_name
    return True


def get_now():
    """
        current date and time object
    """
    return datetime.utcnow()


def get_timestamp():
    """
        current timestamp
    """
    now = get_now()
    return now


def get_indice_name_with_date():
    """
        ES index name
    """
    now = get_now()
    today = now.strftime("%Y-%m-%d")
    return '{name}-{date}'.format(name=ES_INDICE_NAME, date=today)


def process():
    """
        loop process
    """
    global CONFIG

    # Get the timestamp at the beginning
    timestamp = get_timestamp()
    indice_name_with_date = get_indice_name_with_date()

    # Data to send to ES
    current_stats = [timestamp]

    # Grabs stats
    netstat_info = parse_netstat()
    if netstat_info:
        current_stats.extend(netstat_info)

    netdev_info = parse_netdev()
    if netdev_info:
        current_stats.extend(netdev_info)

    tcp_info = parse_proc_net_tcp()
    if tcp_info:
        current_stats.extend([tcp_info])

    if not CONFIG.previous_stats:
        # First iteration, need the next round to compute the derivation
        CONFIG.previous_stats = current_stats
        return

    # Compute the derivations
    stats_derivated = get_derive(CONFIG.previous_stats, current_stats)

    timestamp_for_es = timestamp.strftime("%Y-%m-%dT%H:%M:%S") + ".%03d" % (timestamp.microsecond / 1000) + "Z"

    # Ugly dictionnary redesing because Grafana does not know how to work on nested array objects
    # I'm too lazy to reformat the stats + the derive code
    stats_refactored = []
    common_fields = {
        '@TI': "{0}s".format(TICKTIME),
        '@timestamp': timestamp_for_es,
        '@hostname': HOSTNAME
    }
    if CONFIG.server_roles:
        common_fields.update(CONFIG.server_roles)
    if "netdev" in stats_derivated:
        for elem in stats_derivated["netdev"]:
            dict_temp = copy.deepcopy(common_fields)
            dict_temp.update({"netdev": elem})
            stats_refactored.append(dict_temp)
    if "netstat" in stats_derivated:
        for elem in stats_derivated["netstat"]:
            dict_temp = copy.deepcopy(common_fields)
            dict_temp.update({"netstat": elem})
            stats_refactored.append(dict_temp)
    if "tcpstat" in stats_derivated:
        for elem in stats_derivated["tcpstat"]:
            dict_temp = copy.deepcopy(common_fields)
            dict_temp.update({"tcpstat": elem})
            stats_refactored.append(dict_temp)

    # Replace the prev stats
    CONFIG.previous_stats = current_stats

    # Create the index
    create_index()

    # Ingest stats
    _, bulk_errors = bulk(
        CONFIG.socket_elasticsearch,
        stats_refactored,
        index=indice_name_with_date,
        doc_type=ES_INDICE_NAME,
        raise_on_error=False
    )
    if bulk_errors:
        print("Errors: {}".format(bulk_errors))


def get_derive(prev, current):
    """
        Ugly function to get the differences between prev & curr dicts
    """
    cur = copy.deepcopy(current)  # we don't want to modify original doc

    dt1 = prev[0]
    dt2 = cur[0]
    dt3 = dt2 - dt1
    difftime = dt3.seconds * 1000000 + dt3.microseconds

    # fix timestamp format
    cur[0] = "{0}.{1:03d}Z".format(dt2.strftime("%Y-%m-%dT%H:%M:%S"), (dt2.microsecond / 1000))

    new = {
        "netdev": [],
        "tcpstat": [],
        "netstat": []
    }
    # index 0 is the timestamp info, we start at 1
    i = 1
    while i < len(prev):
        if "netdev" in prev[i]:
            iface = prev[i]["netdev"]["@iface"]
            j = 1
            while j < len(cur):
                if "netdev" in cur[j] and iface == cur[j]["netdev"]["@iface"]:
                    dict_temp = {}
                    for elem in cur[j]["netdev"]:
                        if '@' in elem:
                            dict_temp.update({elem: cur[j]["netdev"][elem]})
                            continue
                        # derivation
                        derivated = round_derive(cur[j]["netdev"][elem], prev[i]["netdev"][elem], difftime)
                        if derivated > 0:
                            dict_temp.update({elem: derivated})
                    new["netdev"].append(dict_temp)
                    break
                j += 1
        if "netstat" in prev[i]:
            extname = prev[i]["netstat"]["@extname"]
            j = 1
            while j < len(cur):
                if "netstat" in cur[j] and extname == cur[j]["netstat"]["@extname"]:
                    dict_temp = {}
                    for elem in cur[j]["netstat"]:
                        if '@' in elem:
                            dict_temp.update({elem: cur[j]["netstat"][elem]})
                            continue
                        # derivation
                        derivated = round_derive(cur[j]["netstat"][elem], prev[i]["netstat"][elem], difftime)
                        if derivated > 0:
                            dict_temp.update({elem: derivated})
                    new["netstat"].append(dict_temp)
                    break
                j += 1
        if "tcpstat" in prev[i]:
            j = 1
            while j < len(cur):
                if "tcpstat" in cur[j]:
                    dict_temp = {}
                    for elem in cur[j]["tcpstat"]:
                        # There is no need to compute the derive here
                        if '@' in elem or cur[j]["tcpstat"][elem] > 0:
                            dict_temp.update({elem: cur[j]["tcpstat"][elem]})
                    new["tcpstat"].append(dict_temp)
                    break
                j += 1
        i += 1
    return new


def round_derive(cur, prev, difftime):
    """
        compute the delta per second
    """
    if float(cur) - float(prev) == 0:
        return 0
    return (float(cur) - float(prev)) / (difftime / 1000 / 1000)


if __name__ == '__main__':
    # ES connection
    CONFIG.socket_elasticsearch = Elasticsearch(CONFIG.es_nodes, port="9200")

    # Get the roles
    CONFIG.server_roles = parse_roles()

    while True:
        THREAD_ = Thread(group=None, target=process, name="process")
        THREAD_.start()

        THREAD_.join()

        time.sleep(TICKTIME)
