deploy_network_stats:
  file.managed:
    - user: root
    - group: root
    - mode: 0400
    - names:
      - /usr/local/bin/scality-network-stats.py:
        - source: salt://scality/network-stats/files/scality-network-stats.py
        - mode: 0544
      - /etc/systemd/system/scality-network-stats.service:
        - source: salt://scality/network-stats/files/scality-network-stats.service
