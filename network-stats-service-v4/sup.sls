{% from "scality/map.jinja" import scality with context %}

include:
 - scality.credentials.installed
 - scality.tracer.curator.installed

deploy_stats_1:
  file.directory:
    - user: root
    - group: root
    - mode: 755
    - name: /var/lib/scality/es_curator_actions
    - mkdirs: True

deploy_stats_2:
  file.managed:
    - user: root
    - group: root
    - mode: 0444
    - names:
      - /usr/share/scality-supervisor/grafana/dashboards/scality-network-dashboard-grfnv5.json:
        - source: salt://scality/network-stats/files/scality-network-dashboard-grfnv5.json
        - mode: 0544
      - /tmp/grafana-config:
        - source: salt://scality/network-stats/files/scality-grafana-config
        - mode: 0500
      - /var/lib/scality/es_curator_actions/delete_network-stats_indices.yml:
        - source: salt://scality/network-stats/files/delete_network-stats_indices.yml
      - /etc/cron.daily/scality-clean-network-stats:
        - source: salt://scality/network-stats/files/clean-network-stats.cron

'python3 /tmp/grafana-config':
  cmd.run
