
Upgrade from network-stats-v2 to network-stats-v4:

Remove all traces of v2:

# salt '*' cmd.run 'rm -f /etc/cron.d/scality-network-stats-cron'
# salt '*' cmd.run 'rm -f /usr/local/bin/scality-network-stats.py'
# curl -XDELETE 'store01:9200/network-stats-v2*'

Install v4 as per the INSTALL.md doc


TODO: what about v3?