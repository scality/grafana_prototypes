Install network-stats-v4:

Network-stats is included from RING v7.4.5
If you are installing or upgrading a 7.4.4.x, here are the instructions:

Download the network-stats-service-v4 from git repo https://bitbucket.org/scality/grafana_prototypes
Upload the whole directory on the supervisor, as network-stats-service-v4

Copy the files and launch the service as such:

mkdir -p /srv/scality/salt/local/scality
mv network-stats-service-v4 /srv/scality/salt/local/scality/network-stats
salt '*' state.sls scality.network-stats
salt -G roles:ROLE_SUP state.sls scality.network-stats.sup

You will need to update a value on the Network-stats-v4 grafana data source:
Change elasticsearch version from 2.x to 6.x and save

