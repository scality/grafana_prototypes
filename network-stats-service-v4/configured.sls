service.systemctl_reload:
  module.run:
    - onchanges:
      - file: /etc/systemd/system/scality-network-stats.service

scality-network-stats:
  service.running:
    - enable: True
    - restart: True
    - watch:
      - file: /usr/local/bin/scality-network-stats.py
      - file: /etc/systemd/system/scality-network-stats.service
